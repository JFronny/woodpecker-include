plugins {
    `java-library`
    application
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

group = "io.gitlab.jfronny"
version = "1.0-SNAPSHOT"

application {
    mainClass.set("io.gitlab.jfronny.woodpecker.include.Main")
}

repositories {
    mavenCentral()
    maven("https://maven.frohnmeyer-wds.de/artifacts")
}

dependencies {
    implementation("net.freeutils:jlhttp:2.6")
    implementation("io.gitlab.jfronny:commons:2022.11.2+21-36-56")
    implementation("io.gitlab.jfronny:commons-gson:2022.11.2+21-36-56")

    api("io.gitlab.jfronny.gson:gson-compile-core:1.0-SNAPSHOT")
    compileOnly("io.gitlab.jfronny.gson:gson-compile-annotations:1.0-SNAPSHOT")
    annotationProcessor("io.gitlab.jfronny.gson:gson-compile-processor:1.0-SNAPSHOT")
}

tasks.runShadow {
    args("8002")
}

tasks.withType<JavaCompile> {
    options.compilerArgs.add("-AgsonCompileNoReflect")
}