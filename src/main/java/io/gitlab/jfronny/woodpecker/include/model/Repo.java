package io.gitlab.jfronny.woodpecker.include.model;

import io.gitlab.jfronny.gson.annotations.SerializedName;
import io.gitlab.jfronny.gson.compile.annotations.GSerializable;

import java.util.List;

@GSerializable
public class Repo {
    public Long id;
    public String owner;
    public String name;
    public String full_name;
    public String avatar_url;
    public String link_url;
    public String clone_url;
    public String default_branch;
    public String scm;
    public Long timeout;
    public String visibility;
    @SerializedName("private")
    public Boolean private$;
    public Boolean trusted;
    public Boolean gated;
    public Boolean active;
    public Boolean allow_pr;
    public String config_file;
    public List<String> cancel_previous_pipeline_events;
}
