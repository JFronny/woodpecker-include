package io.gitlab.jfronny.woodpecker.include.model;

import io.gitlab.jfronny.gson.compile.annotations.GSerializable;

import java.util.List;

@GSerializable
public class RequestModel {
    public Repo repo;
    public Build pipeline;
    public List<Pipeline> configs;
}
