package io.gitlab.jfronny.woodpecker.include;

import io.gitlab.jfronny.commons.HttpUtils;
import io.gitlab.jfronny.commons.log.Logger;
import io.gitlab.jfronny.woodpecker.include.model.*;
import net.freeutils.httpserver.HTTPServer;

import java.io.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class Main {
    public static final Logger LOG = Logger.forName("Woodpecker-Include");

    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            LOG.error("Usage: woodpecker-include <port>");
            return;
        }
        HTTPServer server = new HTTPServer(Integer.parseInt(args[0]));
        HTTPServer.VirtualHost host = server.getVirtualHost(null);
        HttpUtils.setUserAgent("Woodpecker-Include/1.0");
        host.addContext("/ciconfig", (req, resp) -> {
            Ref ref = new Ref();
            try {
                try {
                    return processRequest(req, resp, ref);
                } catch (UncheckedIOException e) {
                    throw e.getCause();
                }
            } catch (Throwable e) {
                if (ref.name == null) LOG.info("Could not process pipeline", e);
                else LOG.info("Could not process pipeline for " + ref.name, e);
                throw e;
            }
        }, "POST");
        server.start();
        LOG.info("Running Woodpecker-Include");
    }

    private static int processRequest(HTTPServer.Request req, HTTPServer.Response resp, Ref ref) throws IOException {
        RequestModel request;
        try (var isr = new InputStreamReader(req.getBody())) {
            request = GC_RequestModel.read(isr);
        }
        ref.name = request.repo.full_name;
        AtomicBoolean changed = new AtomicBoolean(false);
        ResponseModel response = new ResponseModel(request.configs.stream().mapMulti(new PipelineUnpacker(changed)).toList());
        if (changed.get()) {
            resp.sendHeaders(200);
            try (OutputStreamWriter writer = new OutputStreamWriter(resp.getBody())) {
                GC_ResponseModel.write(writer, response);
            }
            LOG.info("Processed includes for pipeline for " + ref.name);
        } else {
            resp.sendHeaders(204);
        }
        return 0;
    }

    private static class Ref {
        public String name;
    }
}
