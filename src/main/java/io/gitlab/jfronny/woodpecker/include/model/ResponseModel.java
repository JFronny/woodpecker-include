package io.gitlab.jfronny.woodpecker.include.model;

import io.gitlab.jfronny.gson.compile.annotations.GSerializable;

import java.util.List;

@GSerializable
public record ResponseModel(List<Pipeline> configs) {
}
