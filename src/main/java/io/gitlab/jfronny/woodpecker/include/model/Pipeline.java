package io.gitlab.jfronny.woodpecker.include.model;

import io.gitlab.jfronny.gson.compile.annotations.GSerializable;

@GSerializable
public record Pipeline(String name, String data) {
}
