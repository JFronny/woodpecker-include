package io.gitlab.jfronny.woodpecker.include.model;

import io.gitlab.jfronny.gson.compile.annotations.GSerializable;

import java.util.List;

@GSerializable
public class Build {
    public Long id;
    public Long number;
    public String author;
    public Long parent;
    public String event;
    public String status;
    public String error;
    public Long enqueued_at;
    public Long created_at;
    public Long updated_at;
    public Long started_at;
    public Long finished_at;
    public String deploy_to;
    public String commit;
    public String branch;
    public String ref;
    public String refspec;
    public String remote;
    public String title;
    public String message;
    public Long timestamp;
    public String sender;
    public String author_avatar;
    public String author_email;
    public String link_url;
    public Boolean signed;
    public Boolean verified;
    public String reviewed_by;
    public Long reviewed_at;
    public List<String> changed_files;
}
